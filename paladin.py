from random import randint

def randi(N):
    return randint(0, N - 1)

p = 1
m = -1
s = 1
a = 2

#load the holy words
infile = open("dict.txt", 'r')

ApList = []
AmList = []
SsList = []
SaList = []
SmList = []
VList = []
xxList = []

curlist = 'xxList'
line = ""
for kwd in ['Ap', 'Am', 'Ss', 'Sa', 'Sm', 'V', 'comments']:
    while line != ('.' + kwd):
        if len(line) > 1:
            if line[0] != '.':
                exec(curlist + '.append(line)')
        line = infile.readline().strip()
    #exec('print ' + curlist)
    curlist = kwd + 'List'

infile.close()
#print 'Finished reading \n \n \n'


#Make a random Adjective or Adverb
def A(pm = 0, v = 0):
    if pm == p: #positive words
        words = ApList
    elif pm == m: #negative words
        words = AmList
    else:
        words = AmList + ApList
    word = words[randi(len(words))]

    if v == 1: #To make Adverb
        if word[-1] == 'y':
            word = word[:-1] + 'ily'
        elif word[-2:] == 'le':
            word = word[:-1] + 'y'
        else:
            word = word + 'ly'
    return word

def S(sa = 0, pl = 0):
    if sa == s:
        words = SsList
    elif sa == a:
        words = SaList
    elif sa == m:
        words = SmList
    else:
        words = SaList + SsList
    word = words[randi(len(words))]

    if pl:
        if word[-1] == 's':
            word = word + 'es'
        elif word[-1] == 'y':
            word = word[:-1] + 'ies'
        else:
            word = word + 's'
    return word

def V(ss = 0):
    words = VList
    word = words[randi(len(words))]
    if ss:
        if word[-1] == 's':
            word = word + 'es'
        elif word[-1] == 'y':
            word = word[:-1] + 'ies'
        else:
            word = word + 's'
    return word

#Sentence roots
roots = [
'V my S',
'V my Ap S',
'V the Sas',
'V the Ap Sas',
'I will V the Sm',
'I will V the Am Sm',
'I will V the Am Sms ',
'I will V the Sm Apv',
'I will V the Sms Apv',
'By the Ap Sas',
'By the Ap Ss',
'Towards Ss',
'Towards Ap Ss',
'Ss Vs the Ss',
'Ss Vs the Ap S',
'V the A S or perish',
'V upon the Ap S',
'the S Vs the A S',
'the A S Vs the S of the Sas',
]


def sentence():
    sent = roots[randi(len(roots))]

    #TODO all of these could be more automatic!
    while 'Sas' in sent:
        sent = sent.replace('Sas', S(a,1), 1)
    while 'Sms' in sent:
        sent = sent.replace('Sms', S(m,1), 1)
    while 'Sss' in sent:
        sent = sent.replace('Sss', S(s,1), 1)
    while 'Sm' in sent:
        sent = sent.replace('Sm', S(m), 1)
    while 'Ss' in sent:
        sent = sent.replace('Ss', S(s), 1)
    while 'Sa' in sent:
        sent = sent.replace('Sa', S(a), 1)
    while 'S' in sent:
        sent = sent.replace('S', S(), 1)
        
    while 'Apv' in sent:
        sent = sent.replace('Apv', A(p,1), 1)
    while 'Amv' in sent:
        sent = sent.replace('Amv', A(m,1), 1)
    while 'Ap' in sent:
        sent = sent.replace('Ap', A(p), 1)
    while 'Am' in sent:
        sent = sent.replace('Am', A(m), 1)
    while 'Av' in sent:
        sent = sent.replace('Av', A(0,1), 1)
    while 'A' in sent:
        sent = sent.replace('A', A(), 1)

    while 'Vs' in sent:
        sent = sent.replace('Vs', V(s), 1)
    while 'V' in sent:
        sent = sent.replace('V', V(), 1)
    
    sent = sent.strip() #remove spaces at start and end

    #add exclamation mark randomly, with higher probability for short sentences
    if randi(len(sent.split()) - 1) == 0:
        sent = sent + '!'
    else:
        sent = sent + '.'
    sent = sent[0].upper() + sent[1:]

    return sent

#Print 20 random sentences
for i in xrange(20):
    print sentence()

"""
terminal> python paladin.py
By the sacred order.
I will guard the dark enemy.
I will shine the wrongdoers magnificently.
Uphold the dark retribution or perish.
Compel the crusades!
I will uphold the heathen enemy.
I will compel the enemies magnificently.
By the sacred champions.
By the great duties.
The humble champion seeks the duty of the champions.
I will bless the wrongdoer.
Strength serves the humble honor!
Purpose upholds the blessed courage!
Compel the blessings!
Light shines the mighty retribution.
The champion guards the noble honor.
Guard my retribution.
Serve my noble purpose.
Towards mighty power.
Condemn the heathen glory or perish.
"""
    
